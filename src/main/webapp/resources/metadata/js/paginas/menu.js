var navegarIndex = -1;
function busqueda() {
    var  buscando = normalizarTexto($("input[id*='buscar-menu']").val());
    if(buscando.length >= 2){
        $('.no-mostrar-busqueda').addClass('active-menuitem');
        $("#menuform").find("li").each(function(){
            var txtMenu = normalizarTexto($(this).find("a").text());
            $(this).addClass('resultado');
            if (txtMenu.indexOf(buscando) > -1) {
              $(this).show();
            } else {
              $(this).hide().removeClass('resultado');
            }
        });
        estaVacio();
    }else{
        $('.no-mostrar-busqueda').removeClass('active-menuitem');
        $("#menuform").find("li").show().removeClass('resultado');
        $('.no-mostrar-busqueda ul').hide();
    }
}

function estaVacio(){
    var ul, li;
    $('.no-mostrar-busqueda').removeClass('resultado');
    $('.no-mostrar-busqueda').each(function(i, el){
        ul = $(el).children('ul');
        ul.show();
        li = $(ul).find('.resultado:visible');
        if ($(li).length == 0){
            $(el).hide();
        }else {
            $(el).show();
        }
    });
}

function resultadosIrSeleccion(i){
    var primerResultado = $('.resultado')[i];
    var url = $(primerResultado).find('a').attr('href');
    if($("input[id*='buscar-menu']").is( ":focus" )) window.location.href = url;  
}

// return texto en minúscula y quita acentos
function normalizarTexto(texto) {
    return texto.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g,"");
}

// Keys
$(document).keydown(function(e) {
    // Para IE obviamos las pulsaciones de teclas
    if($.browser.msie) return;
    
    var  buscando = $("input[id*='buscar-menu']").val().toLowerCase(); 
    
    if (e.keyCode == 66 && e.ctrlKey) {                                 // key Crtl + B        
        $("input[id*='buscar-menu']").focus();
    }else if (e.keyCode == 13 && buscando.length >= 2) {                // key enter   
        resultadosIrSeleccion(navegarIndex == -1? 0:navegarIndex);
    }else if (e.keyCode == 27) {                                        // key escape
        $("input[id*='buscar-menu']").val('').blur();;
        busqueda();
        $('.no-mostrar-busqueda ul').hide();
    }else if (  e.keyCode == 38 && 
                buscando.length >= 2 && 
                navegarIndex > 0){                                      // key flecha arriba
        $('.resultado').css('text-decoration', 'none');
        var navegar = $('.resultado');
        navegarIndex--;
        $(navegar[navegarIndex]).css('text-decoration', 'underline');                
    }else if (  e.keyCode == 40 && 
                buscando.length >= 2 && 
                navegarIndex < $('.resultado').length){                 // key flecha abajo
        $('.resultado').css('text-decoration', 'none');
        var navegar = $('.resultado');
        navegarIndex++;
        $(navegar[navegarIndex]).css('text-decoration', 'underline');
    }

    if (e.keyCode != 38 && e.keyCode != 40){                            // reiniciar navegacion
        $('.resultado').css('text-decoration', 'none');
        navegarIndex = -1;
    }
});

// Para IE eliminamos el buscador debido a sus incompatibilidades
if($.browser.msie) {
    $("input[id*='buscar-menu']").remove();
}