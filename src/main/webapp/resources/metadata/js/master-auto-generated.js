function bloquearCampos() {
    let inputUsuario = PF('usuario_login').jq;
    let inputPassword = PF('password_login').jq;

    inputUsuario.prop('disabled', 'true');
    inputPassword.prop('disabled', 'true');
}

function desbloquearCampos() {
    clearInterval(interval);

    let panelEtiqueta = $("div[id*='panelMensajeErrorConexion']");
    let inputUsuario = PF('usuario_login').jq;
    let inputPassword = PF('password_login').jq;

    inputUsuario.removeProp('disabled');
    inputPassword.removeProp('disabled');

    if (panelEtiqueta.attr('display') !== 'none') {
        panelEtiqueta.hide();
    }
}
