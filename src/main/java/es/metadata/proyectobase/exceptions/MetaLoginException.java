package es.metadata.proyectobase.exceptions;

public class MetaLoginException extends Exception {

    public MetaLoginException(String msg) {
        super(msg);
    }
}