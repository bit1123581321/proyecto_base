package es.metadata.proyectobase.filter;

import es.metadata.proyectobase.security.UsuarioConectado;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;

@WebFilter(filterName = "AuthFilter", urlPatterns = {"*.xhtml"})
public class AuthFilter implements Filter {

    @Inject
    private UsuarioConectado usuarioConectado;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {   
            String paginaLogin = "/login.xhtml";
                        
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;
            HttpSession ses = req.getSession(false);
            
            String reqURI = req.getRequestURI();
            if (reqURI.contains("login_background.xhtml")){
                chain.doFilter(request, response);
            }else{
            
                //Con la segunda condicion detectamos si la url llamanda es la url de inicio sin poner el index.html
                //La barra solo aparece 2 veces (primrera y ultima letra de la url)                        
                if (reqURI.contains(paginaLogin) || (StringUtils.countMatches(reqURI,"/")==2 && reqURI.charAt(0) == '/' && reqURI.charAt(reqURI.length()-1) == '/')) 
                {                
                    //Vamos a la pagina de login, si hay una sesion previa la anulamos
                    if (ses != null) {
                        ses.invalidate();
                    }    

                    chain.doFilter(request, response);
                } 
                else if (reqURI.contains("/javax.faces.resource/"))
                {
                    //Recursos de la pagina
                    chain.doFilter(request, response);
                }
                else {                
                    //Vamos a cualquier pagina menos a la de login

                    if (ses != null && usuarioConectado != null) {                    
                        //El usuario tiene  guardado su perfil en la sesion
                        //Solo se ha podido guardar el perfil si se ha autenticado correctamente
                        chain.doFilter(request, response);  
                    }  
                    else {
                        //Esta intentando entrar a una pagina sin estar autenticado.... 
                        //se le reevia a la pagina de login
                        if (ses != null) {
                            ses.invalidate();
                        }    
                        res.sendRedirect(req.getContextPath() + paginaLogin);
                    }
                }
            }
                                
        } catch (IOException | ServletException t) {
            System.out.println(t.getMessage());
        }
    } 

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
