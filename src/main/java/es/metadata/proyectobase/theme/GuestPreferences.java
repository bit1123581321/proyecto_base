package es.metadata.proyectobase.theme;

import es.metadata.proyectobase.controllers.ApplicationBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@SessionScoped
@Named
public class GuestPreferences implements Serializable {

    private static final long serialVersionUID = 1L;

    private String theme;

    private String layout;

    private boolean overlayMenu = false;

    private boolean slimMenu = false;

    private boolean darkMenu = false;

    @Inject
    private ApplicationBean applicationBean;

    public String getTheme() {
        return theme;
    }

    public String getLayout() {
        return layout;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public boolean isOverlayMenu() {
        return this.overlayMenu;
    }

    public void setOverlayMenu(boolean value) {
        this.overlayMenu = value;
        this.slimMenu = false;
    }

    public boolean isSlimMenu() {
        return this.slimMenu;
    }

    public void setSlimMenu(boolean value) {
        this.slimMenu = value;
    }

    public boolean isDarkMenu() {
        return this.darkMenu;
    }

    public void setDarkMenu(boolean value) {
        this.darkMenu = value;
    }

    @PostConstruct
    public void init() {
        theme = "blue";
        layout = "blue";
    }
}
