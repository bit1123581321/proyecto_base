/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.metadata.proyectobase.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "localDateTimeConverter")
public class LocalDateTimeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            //String format = (String) component.getAttributes().get("format");
            //if (format == null) {
            String format = "dd/MM/yyyy HH:mm:ss";
            //}
            return LocalDateTime.parse(value, DateTimeFormatter.ofPattern(format));
        } catch (DateTimeParseException ex) {
            return "";
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        LocalDateTime dateValue = (LocalDateTime) value;
        String format = (String) component.getAttributes().get("format");
        if (format == null) {
            format = "dd/MM/yyyy HH:mm:ss";
        }
        return dateValue.format(DateTimeFormatter.ofPattern(format));
    }
}
