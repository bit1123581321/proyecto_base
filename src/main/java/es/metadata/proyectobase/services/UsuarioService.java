package es.metadata.proyectobase.services;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import es.metadata.proyectobase.dao.UsuarioMapper;
import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.cdi.Transactional;

public class UsuarioService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UsuarioMapper usuarioMapper;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private LogService logService;

//  !- CRUD -!
    public Integer contarFilas() {
        return usuarioMapper.countAll();
    }

    public List<Usuario> obtenerTodos() {
        return usuarioMapper.selectAll();
    }
    
    public List<Usuario> obtenerTodosConPersona(){
        return usuarioMapper.selectAllConPersona();
    }
    
    public List<Usuario> obtenerTodosConGrupo(){
        return usuarioMapper.selectAllConGrupo();
    }
    
    public Integer contarPorGrupo(int idGrupo){
        return usuarioMapper.countByGrupo(idGrupo);
    }
    
    public List<Usuario> obtenerPorGrupo(int idGrupo){
        return usuarioMapper.selectByGrupo(idGrupo);
    }


    @Transactional
    public void insertar(Usuario usuario) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = usuarioMapper.insert(usuario);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + usuario.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + usuario.toString());

            logService.insertar(log);
        }
    }
    
    @Transactional
    public void insertarConPersona(Usuario usuario) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = usuarioMapper.insertConPersona(usuario);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + usuario.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + usuario.toString());

            logService.insertar(log);
        }
    }

    @Transactional
    public void borrar(Integer idUsuario) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = usuarioMapper.delete(idUsuario);
        } catch (PersistenceException persistanceException) {
            Usuario usuario = obtenerUsuarioPorID(idUsuario);

            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idUsuario, mensajes.getValue("campo0050"), mensajes.getValue("log0094") + ": " + usuario.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0062"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idUsuario, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0095") + ": " + idUsuario);

            logService.insertar(log);
        }
    }

    @Transactional
    public void actualizar(Usuario usuario) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = usuarioMapper.update(usuario);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + usuario.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + usuario.toString());

            logService.insertar(log);
        }
    }
    
    @Transactional
    public void actualizarConGrupo(Usuario usuario) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = usuarioMapper.updateConGrupo(usuario);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + usuario.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), usuario.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + usuario.toString());

            logService.insertar(log);
        }
    }

//  !- Métodos -!
    public Usuario obtenerUsuarioPorID(Integer idUsuario) throws MetaLoginException {
        try {
            return usuarioMapper.selectByPrimaryKey(idUsuario);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idUsuario, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idUsuario);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

    public Usuario obtenerUsuarioPorCorreo(String email) throws MetaLoginException{
        try {
            return usuarioMapper.selectByEmail(email);
        } catch (PersistenceException persistanceException) { //El 5 está metido por fuerza (hay que cambiar)
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 5, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + email);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }
    
    public Usuario obtenerUsuarioPersonaPorCorreo(String email) throws MetaLoginException{
        try {
            return usuarioMapper.selectConPersonaByEmail(email);
        } catch (PersistenceException persistanceException) { //El 5 está metido por fuerza (hay que cambiar)
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 5, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + email);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }
    
    public Usuario obtenerUsuarioConPersona(int idUsuario) throws MetaLoginException{
        try {
            return usuarioMapper.selectConPersona(idUsuario);
        } catch (PersistenceException persistanceException) { //El 5 está metido por fuerza (hay que cambiar)
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 5, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idUsuario);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

    public List<Usuario> obtenerPorNoGrupo(Integer idGrupo) {
        return usuarioMapper.selectByNoGrupo(idGrupo);
    }
}
