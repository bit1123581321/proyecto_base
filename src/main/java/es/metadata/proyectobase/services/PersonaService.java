package es.metadata.proyectobase.services;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import es.metadata.proyectobase.dao.PersonaMapper;
import es.metadata.proyectobase.dao.UsuarioMapper;
import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.model.Persona;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.cdi.Transactional;

public class PersonaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private PersonaMapper personaMapper;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private LogService logService;

//  !- CRUD -!
    public Integer contarFilas() {
        return personaMapper.countAll();
    }

    public List<Persona> obtenerTodos() {
        return personaMapper.selectAll();
    }


    @Transactional
    public void insertar(Persona persona) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = personaMapper.insert(persona);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + persona.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), persona.getIdPersona(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + persona.toString());

            logService.insertar(log);
        }
    }

    @Transactional
    public void borrar(Integer idPersona) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = personaMapper.delete(idPersona);
        } catch (PersistenceException persistanceException) {
            Persona persona = obtenerPersonaPorID(idPersona);

            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idPersona, mensajes.getValue("campo0050"), mensajes.getValue("log0094") + ": " + persona.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0062"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idPersona, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0095") + ": " + idPersona);

            logService.insertar(log);
        }
    }

    @Transactional
    public void actualizar(Persona persona) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = personaMapper.update(persona);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), persona.getIdPersona(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + persona.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), persona.getIdPersona(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + persona.toString());

            logService.insertar(log);
        }
    }

//  !- Métodos -!
    public Persona obtenerPersonaPorID(Integer idPersona) throws MetaLoginException {
        try {
            return personaMapper.selectByPrimaryKey(idPersona);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idPersona, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idPersona);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }
    
    public Persona obtenerPersonaPorDNI(String dni) throws MetaLoginException {
        try {
            return personaMapper.selectByDni(dni);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 5, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + dni);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }
}
