package es.metadata.proyectobase.services;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import es.metadata.proyectobase.dao.FichadaMapper;
import es.metadata.proyectobase.dao.UsuarioMapper;
import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Fichada;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.model.Fichada;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.cdi.Transactional;

public class FichadaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private LogService logService;

    @Inject
    private FichadaMapper fichadaMapper;

//  !- CRUD -!
    public Integer contarFilas() {
        return fichadaMapper.countAll();
    }

    public List<Fichada> obtenerTodos() {
        return fichadaMapper.selectAll();
    }

    @Transactional
    public void insertar(Fichada fichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = fichadaMapper.insert(fichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + fichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), fichada.getIdFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + fichada.toString());

            logService.insertar(log);
        }
    }

    @Transactional
    public void borrar(Integer idFichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = fichadaMapper.delete(idFichada);
        } catch (PersistenceException persistanceException) {
            Fichada fichada = obtenerFichadaPorID(idFichada);

            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idFichada, mensajes.getValue("campo0050"), mensajes.getValue("log0094") + ": " + fichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0062"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idFichada, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0095") + ": " + idFichada);

            logService.insertar(log);
        }
    }

    @Transactional
    public void actualizar(Fichada fichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = fichadaMapper.update(fichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), fichada.getIdFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + fichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), fichada.getIdFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + fichada.toString());

            logService.insertar(log);
        }
    }

//  !- MÃ©todos -!
    public Fichada obtenerFichadaPorID(Integer idFichada) throws MetaLoginException {
        try {
            return fichadaMapper.selectByPrimaryKey(idFichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idFichada, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idFichada);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

    //  !- MÃ©todos -!
    public Fichada obtenerFichadaPorActivo(Usuario user) throws MetaLoginException {
        try {
            return fichadaMapper.selectByActivo(user);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), user.getIdUsuario(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + user);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

    public List<Fichada> obtenerTodoDelUser(Integer idUser) throws MetaLoginException {
        try {
            return fichadaMapper.obtenerTodoDelUser(idUser);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idUser, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idUser);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }
        
         

    public List<Fichada> obtenerTodoDelUserNoActivas(Integer idUser) throws MetaLoginException {
        try {
            return fichadaMapper.obtenerTodoDelUserNoActivas(idUser);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idUser, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idUser);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

}
