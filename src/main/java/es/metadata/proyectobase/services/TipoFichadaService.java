package es.metadata.proyectobase.services;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import es.metadata.proyectobase.dao.TipoFichadaMapper;
import es.metadata.proyectobase.dao.UsuarioMapper;
import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.TipoFichada;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.model.TipoFichada;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.cdi.Transactional;

public class TipoFichadaService implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private LogService logService;
    
    @Inject
    private TipoFichadaMapper tipoFichadaMapper;

//  !- CRUD -!
    public Integer contarFilas() {
        return tipoFichadaMapper.countAll();
    }

    public List<TipoFichada> obtenerTodos() {
        return tipoFichadaMapper.selectAll();
    }


    @Transactional
    public void insertar(TipoFichada tipoFichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = tipoFichadaMapper.insert(tipoFichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + tipoFichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), tipoFichada.getIdTipoFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + tipoFichada.toString());

            logService.insertar(log);
        }
    }

    @Transactional
    public void borrar(Integer idTipoFichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = tipoFichadaMapper.delete(idTipoFichada);
        } catch (PersistenceException persistanceException) {
            TipoFichada tipoFichada = obtenerTipoFichadaPorID(idTipoFichada);

            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idTipoFichada, mensajes.getValue("campo0050"), mensajes.getValue("log0094") + ": " + tipoFichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0062"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idTipoFichada, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0095") + ": " + idTipoFichada);

            logService.insertar(log);
        }
    }

    @Transactional
    public void actualizar(TipoFichada tipoFichada) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = tipoFichadaMapper.update(tipoFichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), tipoFichada.getIdTipoFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + tipoFichada.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), tipoFichada.getIdTipoFichada(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + tipoFichada.toString());

            logService.insertar(log);
        }
    }

//  !- Métodos -!
    public TipoFichada obtenerTipoFichadaPorID(Integer idTipoFichada) throws MetaLoginException {
        try {
            return tipoFichadaMapper.selectByPrimaryKey(idTipoFichada);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idTipoFichada, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idTipoFichada);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

}
