package es.metadata.proyectobase.services;

import es.metadata.proyectobase.dao.LogMapper;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.slf4j.Logger;

public class LogService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private LogMapper logMapper;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private transient Logger logger;

//  !- CRUD -!
    public Integer contarFilas() {
        return logMapper.countAll();
    }
    
    public List<Log> obtenerTodos() {
        try {
            return logMapper.selectAll();
        } catch (PersistenceException persistenceException) {
            logger.debug("Hay algún error de persistencia al intentar OBTENER logs");
        }
        
        return null;
    }

    public void insertar(Log log) {
        try {
            log.setIdLog(contarFilas() + 1);
            
            logMapper.insert(log);
        } catch (PersistenceException persistanceException) {
            logger.debug("Hay algún error de persistencia al intentar INTRODUCIR logs");
        }
    }

    public void actualizar(Log log) {
        try {
            logMapper.update(log);
        } catch (PersistenceException persistenceException) {
            logger.debug("Hay algún error de persistencia al intentar ACTUALIZAR logs");
        }
    }

    public void borrar(Integer idLog) {
        try {
            logMapper.delete(idLog);
        } catch (PersistenceException persistenceException) {
            logger.debug("Hay algún error de persistencia al intentar BORRAR logs");
        }
    }

//  !- Métodos -!
    public Log obtenerLogPorID(Integer idLog) {
        try {
            return logMapper.selectByPrimaryKey(idLog);
        } catch (PersistenceException persistenceException) {
            logger.debug("Hay algún error de persistencia al intentar OBTENER logs");

        }
        
        return null;
    }

}
