package es.metadata.proyectobase.services;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import es.metadata.proyectobase.dao.GrupoMapper;
import es.metadata.proyectobase.dao.UsuarioMapper;
import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Grupo;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.model.Log;
import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.cdi.Transactional;

public class GrupoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    GrupoMapper grupoMapper;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    private LogService logService;

//  !- CRUD -!
    public Integer contarFilas() {
        return grupoMapper.countAll();
    }

    public List<Grupo> obtenerTodos() {
        return grupoMapper.selectAll();
    }

    @Transactional
    public void insertar(Grupo grupo) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = grupoMapper.insert(grupo);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), 0, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0092") + ": " + grupo.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0061"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), grupo.getIdGrupo(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0093") + ": " + grupo.toString());

            logService.insertar(log);
        }
    }

    @Transactional
    public void borrar(Integer idGrupo) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = grupoMapper.delete(idGrupo);
        } catch (PersistenceException persistanceException) {
            Grupo grupo = obtenerGrupoPorId(idGrupo);

            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idGrupo, mensajes.getValue("campo0050"), mensajes.getValue("log0094") + ": " + grupo.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0062"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idGrupo, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0095") + ": " + idGrupo);

            logService.insertar(log);
        }
    }

    @Transactional
    public void actualizar(Grupo grupo) throws MetaLoginException {
        Integer respuesta = 0;

        try {
            respuesta = grupoMapper.update(grupo);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), grupo.getIdGrupo(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0096") + ": " + grupo.toString());

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0063"));
        }

        if (respuesta != 0) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), grupo.getIdGrupo(), mensajes.getValue("campo0050"),
                    mensajes.getValue("log0097") + ": " + grupo.toString());

            logService.insertar(log);
        }
    }

//  !- Métodos -!
    public Grupo obtenerGrupoPorId(Integer idGrupo) throws MetaLoginException {
        try {
            return grupoMapper.selectByPrimaryKey(idGrupo);
        } catch (PersistenceException persistanceException) {
            Log log = new Log(LogEntidadEnum.USUARIO.getCodigo(), idGrupo, mensajes.getValue("campo0050"),
                    mensajes.getValue("log0098") + ": " + idGrupo);

            logService.insertar(log);

            throw new MetaLoginException(mensajes.getValue("excepcion0064"));
        }
    }

}
