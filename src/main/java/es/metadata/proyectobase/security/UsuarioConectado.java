package es.metadata.proyectobase.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value = "usuarioConectado")
@SessionScoped
public class UsuarioConectado implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String PERFIL;
    private String usuario; 
    private String nombreUsuario; 
    private String rol;   
    private Boolean modo_dios;
    private Integer idUsuario;
    private List<Integer> permisos = new ArrayList<>();
    private String esquema;
    private int idEsquema;
    private int idSubcontrata;
    private int idCliente;
    private String paginaActualMetacontratas="";
    private List<Integer> visibilidadPorEsquema;

    public Boolean isModo_dios() {
        return modo_dios;
    }

    public void setModo_dios(Boolean modo_dios) {
        this.modo_dios = modo_dios;
    }

    public String getPERFIL() {
        return PERFIL;
    }

    public void setPERFIL(String PERFIL) {
        this.PERFIL = PERFIL;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public List<Integer> getPermisos() {
        return permisos;
    }

    public void setPermisos(List<Integer> permisos) {
        this.permisos = permisos;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEsquema() {
        return esquema;
    }

    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }

    public int getIdEsquema() {
        return idEsquema;
    }

    public void setIdEsquema(int idEsquema) {
        this.idEsquema = idEsquema;
    }

    public int getIdSubcontrata() {
        return idSubcontrata;
    }

    public void setIdSubcontrata(int idSubcontrata) {
        this.idSubcontrata = idSubcontrata;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getPaginaActualMetacontratas() {
        return paginaActualMetacontratas;
    }

    public void setPaginaActualMetacontratas(String paginaActualMetacontratas) {
        this.paginaActualMetacontratas = paginaActualMetacontratas;
    }

    public List<Integer> getVisibilidadPorEsquema() {
        return visibilidadPorEsquema;
    }

    public void setVisibilidadPorEsquema(List<Integer> visibilidadPorEsquema) {
        this.visibilidadPorEsquema = visibilidadPorEsquema;
    }
    
    @Override
    public String toString() {
        return usuario;
    }        
}
