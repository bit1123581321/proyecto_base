package es.metadata.proyectobase.security;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.event.AbortProcessingException;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.exceptions.PersistenceException;

@Interceptor
@SecurityPermission
public class SecurityPermissionInterceptor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UsuarioConectado perfilUsuario;

    @AroundInvoke
    public Object intercept(InvocationContext context) {
        if (perfilUsuario == null) {
            throw new SecurityException();
        }

        if (perfilUsuario.isModo_dios()) {
            //Si el usuario es un dios, no miramos los permisos
            return ejecutarMetodo(context);
        } else {

            //Extraemos los permismos de los metodos
            List<Integer> permisosPermitidos = extraerPermisos(context.getMethod());
            if (permisosPermitidos == null) {
                //Si se ha lanzado el interceptor pero no es el metodo el que lo ha lanzado
                //es que lo ha lanzado la clase
                permisosPermitidos = extraerPermisos(context.getTarget().getClass());
            }

            if (!Collections.disjoint(perfilUsuario.getPermisos(), permisosPermitidos)) {
                return ejecutarMetodo(context);
            } else {
                throw new SecurityException();
            }
        }
    }

    private Object ejecutarMetodo(InvocationContext context) {
        try {
            return context.proceed();
        } catch (AbortProcessingException | PersistenceException | NullPointerException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new SecurityException();
        }
    }

    private List<Integer> extraerPermisos(Method m) {
        if (m.isAnnotationPresent(SecurityPermission.class)) {
            SecurityPermission security = m.getAnnotation(SecurityPermission.class);
            List<Integer> aux = new ArrayList<>();
            for (int i : security.permisosPermitidos()) {
                aux.add(i);
            }
            return aux;
        }
        return null;
    }

    private List<Integer> extraerPermisos(Class c) {
        if (c.isAnnotationPresent(SecurityPermission.class)) {
            SecurityPermission security = (SecurityPermission) c.getAnnotation(SecurityPermission.class);
            List<Integer> aux = new ArrayList<>();
            for (int i : security.permisosPermitidos()) {
                aux.add(i);
            }
            return aux;
        }
        return null;
    }
}
