/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.metadata.proyectobase.security;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.exceptions.PersistenceException;

@Interceptor
@SecurityRol
public class SecurityRolInterceptor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UsuarioConectado perfilUsuario;

    @PostConstruct
    public void intercept(InvocationContext context) throws SecurityException, BindingException {

        if (perfilUsuario == null) {
            throw new SecurityException();
        }
        List<String> rolesPermitidos = extraerRoles(context.getTarget().getClass());
        if (rolesPermitidos.contains(perfilUsuario.getRol())) {
            try {
                context.proceed();
            } catch (BindingException ex) {
                throw ex;
            } catch (PersistenceException | NullPointerException ex) {
                throw ex;
            } catch (Exception ex) {
                throw new SecurityException();
            }
        } else {
            throw new SecurityException();
        }
    }

    private List<String> extraerRoles(Class c) {
        if (c.isAnnotationPresent(SecurityRol.class)) {
            SecurityRol security = (SecurityRol) c.getAnnotation(SecurityRol.class);
            return Arrays.asList(security.rolesPermitidos());
        }
        return null;
    }
}
