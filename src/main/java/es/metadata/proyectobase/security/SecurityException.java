package es.metadata.proyectobase.security;

import java.io.Serializable;

public class SecurityException extends RuntimeException implements Serializable{

    private static final long serialVersionUID = 1L;

    public SecurityException() {
    }

    public SecurityException(String msg) {
        super(msg);
    }
}
