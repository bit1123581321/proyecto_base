package es.metadata.proyectobase.dao;

import es.metadata.proyectobase.model.Fichada;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;
import es.metadata.proyectobase.model.Usuario;

@Mapper
public interface FichadaMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<Fichada> selectAll();
    
    Integer insert(Fichada fichada);

    Integer update(Fichada fichada);

    Integer delete(@Param("idFichada") Integer idFichada);
    
//  !- CONSULTAS -!
    Fichada selectByPrimaryKey(@Param("idFichada") Integer idFichada);

    List<Fichada> obtenerTodoDelUser(@Param("idUsuario") Integer idUser);
    
    List<Fichada> obtenerTodoDelUserNoActivas(@Param("idUsuario") Integer idUser);
    
    Fichada selectByActivo(@Param("usuario") Usuario usuario);
}
