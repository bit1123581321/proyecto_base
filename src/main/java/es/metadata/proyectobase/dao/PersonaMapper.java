package es.metadata.proyectobase.dao;

import es.metadata.proyectobase.model.Persona;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;

@Mapper
public interface PersonaMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<Persona> selectAll();
    
    Integer insert(Persona persona);

    Integer update(Persona persona);

    Integer delete(@Param("idPersona") Integer idPersona);
    
//  !- CONSULTAS -!
    Persona selectByPrimaryKey(@Param("idPersona") Integer idPersona);
    
    Persona selectByDni(@Param("dni") String dni);
    

}
