package es.metadata.proyectobase.dao;

import es.metadata.proyectobase.model.Usuario;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;

@Mapper
public interface UsuarioMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<Usuario> selectAll();
    
    List<Usuario> selectAllConPersona();
    
    List<Usuario> selectAllConGrupo();
    
    Integer insert(Usuario usuario);
    
    Integer insertConPersona(Usuario usuario);

    Integer update(Usuario usuario);
    
    Integer updateConGrupo(Usuario usuario);

    Integer delete(@Param("idUsuario") Integer idUsuario);
    
//  !- CONSULTAS -!
    Usuario selectByPrimaryKey(@Param("idUsuario") Integer idUsuario);
    
    Usuario selectByEmail(@Param("email") String email);
    
    Usuario selectConPersona(@Param("idUsuario") Integer idUsuario);
    
    Usuario selectConPersonaByEmail(@Param("email") String email);
    
    Integer countByGrupo(@Param("idGrupo") Integer idGrupo);

    public List<Usuario> selectByGrupo(@Param("idGrupo") Integer idGrupo);

    public List<Usuario> selectByNoGrupo(@Param("idGrupo") Integer idGrupo);

}
