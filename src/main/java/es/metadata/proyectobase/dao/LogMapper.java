package es.metadata.proyectobase.dao;

import es.metadata.proyectobase.model.Log;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;

@Mapper
public interface LogMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<Log> selectAll();
    
    Integer insert(Log log);

    Integer update(Log log);

    Integer delete(@Param("idLog") Integer idLog);
    
//  !- CONSULTAS -!
    Log selectByPrimaryKey(@Param("idLog") Integer idLog);

}
