package es.metadata.proyectobase.dao;
import es.metadata.proyectobase.model.TipoFichada;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;

@Mapper
public interface TipoFichadaMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<TipoFichada> selectAll();
    
    Integer insert(TipoFichada tipoTipoFichada);

    Integer update(TipoFichada tipoTipoFichada);

    Integer delete(@Param("idTipoFichada") Integer idTipoFichada);
    
//  !- CONSULTAS -!
    TipoFichada selectByPrimaryKey(@Param("idTipoFichada") Integer idTipoFichada);
}
