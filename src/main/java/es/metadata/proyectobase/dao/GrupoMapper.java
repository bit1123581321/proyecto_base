package es.metadata.proyectobase.dao;

import es.metadata.proyectobase.model.Grupo;
import es.metadata.proyectobase.model.Usuario;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.cdi.Mapper;

@Mapper
public interface GrupoMapper {
    
//  !- CRUD -!
    Integer countAll();
    
    List<Grupo> selectAll();
    
    List<Grupo> selectAllConPersona();
    
    Integer insert(Grupo grupo);

    Integer update(Grupo grupo);

    Integer delete(@Param("idGrupo") Integer idGrupo);
    
//  !- CONSULTAS -!
    Grupo selectByPrimaryKey(@Param("idGrupo") Integer idGrupo);

}
