package es.metadata.proyectobase.validators;

import es.metadata.proyectobase.providers.MessageFactoryProvider;
import es.metadata.proyectobase.services.UsuarioService;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import org.primefaces.validate.ClientValidator;

@FacesValidator(value = "EmailUsuarioUnicoValidator")
public class EmailUsuarioUnicoValidator implements Validator, ClientValidator {

    @Inject
    private MessageFactoryProvider mensajes;

    @Inject
    private UsuarioService usuarioService;

    public MessageFactoryProvider getMensajes() {
        return mensajes;
    }

    public void setMensajes(MessageFactoryProvider mensajes) {
        this.mensajes = mensajes;
    }

    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //AQUÍ VA LA LÓGICA DEL VALIDADOR
    }

    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    @Override
    public String getValidatorId() {
        return "EmailUsuarioUnicoValidator";
    }

}
