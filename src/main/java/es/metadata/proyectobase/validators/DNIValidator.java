package es.metadata.proyectobase.validators;

import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import org.primefaces.validate.ClientValidator;

@FacesValidator("DNIValidator")
public class DNIValidator implements Validator, ClientValidator {

    private static final String NIF_STRING_ASOCIATION = "TRWAGMYFPDXBNJZSQVHLCKET";
    //Los DNI que comiencen por alguna de estas letras, son DNI de 7 numeros
    private static final String NIF_ESPECIALES_7_LETRAS = "LKXYZM";

    @Inject
    private MessageFactoryProvider mensajes;

    public MessageFactoryProvider getMensajes() {
        return mensajes;
    }

    public void setMensajes(MessageFactoryProvider mensajes) {
        this.mensajes = mensajes;
    }

    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    @Override
    public String getValidatorId() {
        return "CIFValidator";
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Integer idPais = (Integer) component.getAttributes().get("idPais");
        if (idPais == null) {
            //Si le pasamos el atributo a un componente personalizado
            idPais = (Integer) component.getNamingContainer().getAttributes().get("idPais");
        }
        //if (idPais != null && idPais != Pais.ID_ESPANA) {------------------------------------------------------------------------------ERROR DE IMPORT, PAIS NO EXISTE
            //No comprobamos el formato del cif para empresas que no son de españa
          //  return;
        //}
        String DNI = value.toString().toUpperCase();
        if (!compruebaDNI(DNI)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", mensajes.getValue("campo0348")));
        }
    }
    /**
     * Comprueba si un dni es correcto (ejemplo de DNI: 12345678-J). También
     * comprueba NIE (Número de Identificación de Extranjero). Estos se ajustan
     * a los patrones: X12345678A o X1234567A
     */
    public static boolean compruebaDNI(String cadenaDNI) {
        try {
            cadenaDNI = cadenaDNI.trim().toUpperCase();
            //La longitud de todos los DNI es 9, salvo para los NIE emitidos con
            //anterioridad a la entrada en vigor de la orden INT/2058/2008, que
            //tienen una longitud de 10. Los pasaporte pueden tener menos cifras
            //(al menos 8)
            int cl = cadenaDNI.length();
            if ((cl == 8) || (cl == 9) || (cl == 10)) {
                String numeroDNI;
                //Comprobamos si se trata de un NIE antiguo (10 dígitos). Después
                //comprobamos si el DNI que vamos a comprobar, es el normal de 8 numeros o es especial.
                if (cl == 10) {
                    //10 caracteres
                    if ((cadenaDNI.charAt(0) != 'X') && (cadenaDNI.charAt(0) != 'Y')) {
                        //debe comenzar con X o con Y
                        return false;
                    } else {
                        numeroDNI = cadenaDNI.substring(1, 9);
                    }
                } else if ((cl == 8) && (Character.isLetter(cadenaDNI.charAt(cl - 1)))) {
                    //8 caracteres y el último es una letra
                    numeroDNI = cadenaDNI.substring(0, 7);
                } else if (NIF_ESPECIALES_7_LETRAS.indexOf(cadenaDNI.charAt(0)) == -1) {
                    //9 caracteres y el primero no es una letra
                    numeroDNI = cadenaDNI.substring(0, 8);
                } else {
                    //9 caracteres y el primero es una letra. Si dicha letra es una
                    //de XYZ, se sustituye por un 0,1 y 2 respectivamente
                    numeroDNI = cadenaDNI.substring(1, 8);
                    char primera = cadenaDNI.charAt(0);
                    switch (primera) {
                        case 'X':
                            numeroDNI = "0" + numeroDNI;
                            break;
                        case 'Y':
                            numeroDNI = "1" + numeroDNI;
                            break;
                        case 'Z':
                            numeroDNI = "2" + numeroDNI;
                            break;
                    }
                }
                //Comprobamos si no hay caracteres no numericos por medio del numero
                //El mayor que cero es para evitar un casque que se producia al introducir dni con el formato X-6780471W
                if (isNumeric(numeroDNI) && Integer.parseInt(numeroDNI) > 0) {
                    int numero = Integer.parseInt(numeroDNI);
                    char letraDNI;
                    if (cl == 9) {
                        letraDNI = cadenaDNI.charAt(8);
                    } else if (cl == 8) {
                        letraDNI = cadenaDNI.charAt(7);
                    } else {
                        letraDNI = cadenaDNI.charAt(9);
                    }
                    char letraDNICalculada = NIF_STRING_ASOCIATION.charAt(numero % 23);
                    if (letraDNI == letraDNICalculada) {
                        //La letra de verificacion coincide con la introducida, por lo que el DNI es correcto
                        return true;
                    } else {
                        //La letra de verificacion NO coincide con la introducida, por lo que el DNI es correcto
                        return false;
                    }
                } else {
                    //El DNI tiene caracteres no numéricos inválidos
                    return false;
                }
            } else {
                //El DNI no tiene las 9 letras o numeros
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Comprueba si la cadena representa a un número
     */
    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Devuelve un DNI aleatorio válido
     */
    public static String getDNIAleatorio() {
        Random rnd = new Random();
        String res = "";
        int num = rnd.nextInt(100000000);
        DecimalFormat df = new DecimalFormat("00000000");
        int iletra = num % 23;
        res = df.format(num) + Character.toUpperCase(NIF_STRING_ASOCIATION.charAt(iletra));
        return res;
    }

    /**
     * Devuelve un NIE antiguo aleatorio
     */
    public static String getNIEAleatorioAnt() {
        Random rnd = new Random();
        String res = "";
        int num = rnd.nextInt(100000000);
        DecimalFormat df = new DecimalFormat("00000000");
        int iletra = num % 23;
        List letraInicial = Arrays.asList(new String[]{"X", "Y"});
        Collections.shuffle(letraInicial);
        res = letraInicial.get(0) + df.format(num) + Character.toUpperCase(NIF_STRING_ASOCIATION.charAt(iletra));
        return res;
    }

    /**
     * Devuelve un NIE nuevo aleatorio
     */
    public static String getNIEAleatorioAct() {
        Random rnd = new Random();
        String res = "";
        int num = rnd.nextInt(10000000);
        DecimalFormat df = new DecimalFormat("0000000");
        int iletra = num % 23;
        List letraInicial = Arrays.asList(new String[]{"X", "Y"});
        Collections.shuffle(letraInicial);
        res = letraInicial.get(0) + df.format(num) + Character.toUpperCase(NIF_STRING_ASOCIATION.charAt(iletra));
        return res;
    }

    public static char getLetraDNI(int numeroDNI) {
        return NIF_STRING_ASOCIATION.charAt(numeroDNI % 23);
    }

    public static char getLetraDNI(String cadenaDNI) {
        String numeroDNI = "";
        if (cadenaDNI.length() == 8) {
            numeroDNI = cadenaDNI.substring(1, 8);
            char primera = cadenaDNI.charAt(0);
            switch (primera) {
                case 'X':
                    numeroDNI = "0" + numeroDNI;
                    break;
                case 'Y':
                    numeroDNI = "1" + numeroDNI;
                    break;
                case 'Z':
                    numeroDNI = "2" + numeroDNI;
                    break;
            }
        } else if (cadenaDNI.length() == 9) {
            numeroDNI = cadenaDNI.substring(1, 9);
        }
        return NIF_STRING_ASOCIATION.charAt(Integer.parseInt(numeroDNI) % 23);

    }
}
