package es.metadata.proyectobase.validators;

import java.util.Map;
import java.util.regex.Pattern;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.validate.ClientValidator;

@FacesValidator("PasswordValidator")
public class PasswordValidator implements Validator, ClientValidator {

    private final Pattern pattern;
    private static final String PASSWORD_PATTERN = "(((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])).{8,64})";

    public PasswordValidator() {
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //AQUÍ VA LA LÓGICA DEL VALIDADOR
    }

    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    @Override
    public String getValidatorId() {
        return "PasswordValidator";
    }
}
