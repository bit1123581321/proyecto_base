package es.metadata.proyectobase.validators;

import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import org.primefaces.validate.ClientValidator;

@FacesValidator("CIFValidator")
public class CIFValidator implements Validator, ClientValidator {

    private static final String CIF_CON_CARACTER_CONTROL_LETRA = "NPQRSW";
    private static final String CIF_CON_CARACTER_CONTROL_NUMERICO = "ABCDEFGHJUV";
    private static final String[] LETRAS_CONTROL_CIF = new String[]{"J", "A", "B", "C", "D", "E", "F", "G", "H", "I"};

    @Inject
    private MessageFactoryProvider mensajes;

    public MessageFactoryProvider getMensajes() {
        return mensajes;
    }

    public void setMensajes(MessageFactoryProvider mensajes) {
        this.mensajes = mensajes;
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //AQUÍ VA LA LÓGICA DEL VALIDADOR
    }

    @Override
    public Map<String, Object> getMetadata() {
        return null;
    }

    @Override
    public String getValidatorId() {
        return "CIFValidator";
    }

}
