package es.metadata.proyectobase.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TipoFichada implements Serializable{

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    private Integer idTipoFichada;
    
    @NotNull
    @Size(max = 255)
    private String nombre;

    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Get & Set">

    public Integer getIdTipoFichada() {
        return idTipoFichada;
    }

    public void setIdTipoFichada(Integer idTipoFichada) {
        this.idTipoFichada = idTipoFichada;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
  


    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoFichada other = (TipoFichada) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.idTipoFichada, other.idTipoFichada)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.idTipoFichada);
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }
    
    
    @Override
    public String toString() {
        return "Usuario{" + "idTipoFichada=" + idTipoFichada + ", nombre=" + nombre+"}";
    }
    // </editor-fold>
}