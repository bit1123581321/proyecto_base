package es.metadata.proyectobase.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Grupo implements Serializable{

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    private Integer idGrupo;
    
    @NotNull
    @Size(max = 255)
    private String nombre;

    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Get & Set">

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
  


    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Grupo other = (Grupo) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.idGrupo, other.idGrupo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.idGrupo);
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }
    
    
    @Override
    public String toString() {
        return "Usuario{" + "idGrupo=" + idGrupo + ", nombre=" + nombre+"}";
    }
    // </editor-fold>
}