package es.metadata.proyectobase.model;

import es.metadata.proyectobase.constantes.LogEntidadEnum;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Log implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer idLog;

    @NotNull
    @Size(max = 2)
    private String entidad;

    @NotNull
    private Integer idEntidad;

    @Size(max = 2)
    private String entidad2;

    private Integer idEntidad2;

    @NotNull
    private LocalDateTime fecha;

    @NotNull
    @Size(max = 255)
    private String usuario;

    @NotNull
    @Size(max = 1024)
    private String mensaje;

//  !-- Relaciones --!
    private LogEntidadEnum logEntidad;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Get & Set">
    public Integer getIdLog() {
        return idLog;
    }

    public void setIdLog(Integer idLog) {
        this.idLog = idLog;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Integer getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(Integer idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getEntidad2() {
        return entidad2;
    }

    public void setEntidad2(String entidad2) {
        this.entidad2 = entidad2;
    }

    public Integer getIdEntidad2() {
        return idEntidad2;
    }

    public void setIdEntidad2(Integer idEntidad2) {
        this.idEntidad2 = idEntidad2;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public LogEntidadEnum getLogEntidad() {
        return logEntidad;
    }

    public void setLogEntidad(LogEntidadEnum logEntidad) {
        this.logEntidad = logEntidad;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Métodos">
    public Log() {
    }

    public Log(String entidad, Integer idEntidad, String usuario, String mensaje) {
        this.entidad = entidad;
        this.idEntidad = idEntidad;
        this.usuario = usuario;
        this.mensaje = mensaje;
        this.fecha = LocalDateTime.now();
    }

    public Log(String entidad, Integer idEntidad, String entidad2, Integer idEntidad2, String usuario, String mensaje) {
        this.entidad = entidad;
        this.idEntidad = idEntidad;
        this.entidad2 = entidad2;
        this.idEntidad2 = idEntidad2;
        this.usuario = usuario;
        this.mensaje = mensaje;
        this.fecha = LocalDateTime.now();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idLog);
        hash = 97 * hash + Objects.hashCode(this.entidad);
        hash = 97 * hash + Objects.hashCode(this.idEntidad);
        hash = 97 * hash + Objects.hashCode(this.entidad2);
        hash = 97 * hash + Objects.hashCode(this.idEntidad2);
        hash = 97 * hash + Objects.hashCode(this.fecha);
        hash = 97 * hash + Objects.hashCode(this.usuario);
        hash = 97 * hash + Objects.hashCode(this.mensaje);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Log other = (Log) obj;
        if (!Objects.equals(this.entidad, other.entidad)) {
            return false;
        }
        if (!Objects.equals(this.entidad2, other.entidad2)) {
            return false;
        }
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.mensaje, other.mensaje)) {
            return false;
        }
        if (!Objects.equals(this.idLog, other.idLog)) {
            return false;
        }
        if (!Objects.equals(this.idEntidad, other.idEntidad)) {
            return false;
        }
        if (!Objects.equals(this.idEntidad2, other.idEntidad2)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Log{" + "idLog=" + idLog + ", entidad=" + entidad + ", idEntidad=" + idEntidad + ", entidad2=" + entidad2 + ", idEntidad2=" + idEntidad2 + ", fecha=" + fecha + ", usuario=" + usuario + ", mensaje=" + mensaje + '}';
    }
    // </editor-fold>

}
