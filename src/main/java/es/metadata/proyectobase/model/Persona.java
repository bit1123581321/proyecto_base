package es.metadata.proyectobase.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Id;

public class Persona implements Serializable{

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer idPersona;
       

    private String nombre;
    
    private String apellidos;

    private String dni;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Get & Set">

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.idPersona);
        hash = 31 * hash + Objects.hashCode(this.nombre);
        hash = 31 * hash + Objects.hashCode(this.apellidos);
        hash = 31 * hash + Objects.hashCode(this.dni);
        return hash;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellidos, other.apellidos)) {
            return false;
        }
        if (!Objects.equals(this.dni, other.dni)) {
            return false;
        }
        if (!Objects.equals(this.idPersona, other.idPersona)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona{" + "idUsuario=" + idPersona +  ", nombre=" + nombre + ", apellidos=" + apellidos+", dni=" + dni+ "}";
    }
    // </editor-fold>
}