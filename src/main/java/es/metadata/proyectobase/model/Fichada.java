package es.metadata.proyectobase.model;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.time.DateUtils;

public class Fichada implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Variables">
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer idFichada;

    @NotNull
    @Size(max = 255)
    private Date fechaInicio;

    private Integer duracion;

    private Usuario usuario;

    private TipoFichada tipoFichada;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Get & Set">
    public Integer getIdFichada() {
        return idFichada;
    }

    public void setIdFichada(Integer idFichada) {
        this.idFichada = idFichada;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoFichada getTipoFichada() {
        return tipoFichada;
    }

    public void setTipoFichada(TipoFichada tipoFichada) {
        this.tipoFichada = tipoFichada;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Métodos">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.idFichada);
        hash = 83 * hash + Objects.hashCode(this.fechaInicio);
        hash = 83 * hash + Objects.hashCode(this.duracion);
        hash = 83 * hash + Objects.hashCode(this.usuario);
        hash = 83 * hash + Objects.hashCode(this.tipoFichada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fichada other = (Fichada) obj;
        if (!Objects.equals(this.idFichada, other.idFichada)) {
            return false;
        }
        if (!Objects.equals(this.fechaInicio, other.fechaInicio)) {
            return false;
        }
        if (!Objects.equals(this.duracion, other.duracion)) {
            return false;
        }
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.tipoFichada, other.tipoFichada)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idFichada=" + idFichada + ", duracion=" + duracion + "}";
    }

    public long tiempo() throws ParseException, MetaLoginException {
        long tiempo = 0;
        Date empezo = this.getFechaInicio();
        Date actual = new Date();
        tiempo = (int) (actual.getTime() - empezo.getTime()) / DateUtils.MILLIS_PER_SECOND;

        return tiempo;
    }

    public String tiempoEscrito() throws ParseException, MetaLoginException {
        long tiempo = 0;
        if (this.duracion != 0) {
            tiempo = duracion;
        } else {
            tiempo = tiempo();
        }

        long horas = tiempo / 3600;
        long minutos = (tiempo - horas * 3600) / 60;

        String escrito = horas + " horas y " + minutos + " minutos";
        return escrito;
    }
    
    public String fechaInicioParseada() throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(fechaInicio);
    }
    // </editor-fold>
}
