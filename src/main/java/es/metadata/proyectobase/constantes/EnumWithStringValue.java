package es.metadata.proyectobase.constantes;

import java.io.Serializable;

public interface EnumWithStringValue extends Serializable {

    public String getValue();

}
