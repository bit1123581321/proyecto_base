package es.metadata.proyectobase.constantes;

import java.io.Serializable;

public interface EnumWithIntegerValue extends Serializable {

    public Integer getValue();

}
