package es.metadata.proyectobase.constantes;

public enum TipoHost {

    PRODUCCION("P"),
    DESARROLLO("D"),
    JENKINS("J");

    private final String code;

    TipoHost(String code) {
        this.code = code;
    }

    public String getCodigo() {
        return code;
    }
}
