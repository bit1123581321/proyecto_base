package es.metadata.proyectobase.constantes;

public enum LogEntidadEnum {

    APLICACION("AP"),
    LOG_ENTIDAD("LE"),
    LOG_ENTRADA_APLICACION("LA"),
    LOG_ENTRADA("LI"),
    LOG("LO"),
    PASSWORD("PW"),
    USUARIO_ACCESO("UA"),
    USUARIO("US");

    private final String code;

    LogEntidadEnum(String code) {
        this.code = code;
    }

    public String getCodigo() {
        return code;
    }
}
