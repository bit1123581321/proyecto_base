package es.metadata.proyectobase.rest;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;

/**
 * Jackson JSON processor could be controlled via providing a custom Jackson
 * ObjectMapper instance. This could be handy if you need to redefine the
 * default Jackson behavior and to fine-tune how your JSON data structures look
 * like (copied from Jersey web site).
 *
 *
 * @see https://jersey.java.net/documentation/latest/media.html#d0e4799
 */
@Provider
public class MyJacksonJsonProvider implements ContextResolver<ObjectMapper> {

    @Inject
    private transient Logger log;
        
    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        //No lo se...
        //MAPPER.disable(MapperFeature.USE_GETTERS_AS_SETTERS);

        MAPPER.setSerializationInclusion(Include.ALWAYS);   //Pera que me incluya las propiedades nulas        

        //Esta no lo se... me cascaba al poner que incluyera los nulos
        MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        //MAPPER.configure(SerializationFeature., false);      
        //Esto es para que no use los constructures, ni los setters y getters (porque me lanza la carga diferida)
        MAPPER.setVisibility(MAPPER.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE)
                .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
        
        //Hacemos que ignore la popiedad handler... que saca informacion de MyBatis sobre las relaciones
        MAPPER.addMixIn(Object.class, PropertyFilterMixIn.class);
                
        //El filtro que estamos añadiento es la la clase PropertyFilterMixIn con la que hacemos mixin
        FilterProvider filters = new SimpleFilterProvider().addFilter("filter properties by name",SimpleBeanPropertyFilter.serializeAllExcept("handler"));          
        MAPPER.setFilterProvider(filters);       
    }

    public MyJacksonJsonProvider() {
        System.out.println("Instantiate MyJacksonJsonProvider");
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
        System.out.println("MyJacksonProvider.getContext() called with type: " + type);
        return MAPPER;
    }
}
