/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.metadata.proyectobase.rest;

import com.fasterxml.jackson.annotation.JsonFilter;

/**
 *
 * @author ivan.caamano
 */
@JsonFilter("filter properties by name")
class PropertyFilterMixIn {}