/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.metadata.proyectobase.rest;

/**
 *
 * @author ivan.caamano
 * @param <E>
 */
public class Resultado<E> {
   
    private boolean error;
    
    private String mensaje;
    
    private E resultado;

    public Resultado() {
        mensaje = "OK";
        error = false;
    }
    
    public Resultado(E resultado) {
        this();        
        this.resultado = resultado;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public E getResultado() {
        return resultado;
    }

    public void setResultado(E resultado) {
        this.resultado = resultado;
    }
    
    
}
