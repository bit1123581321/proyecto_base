package es.metadata.proyectobase.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

public class Hash {

    private static final char[] HEX_CHARS = {'0', '1', '2', '3',
        '4', '5', '6', '7',
        '8', '9', 'a', 'b',
        'c', 'd', 'e', 'f',};

    /**
     * Obtiene el hash MD5 de la cadena de caracteres que se le pasa como
     * argumento
     */
    public static String calcular(String cadena) {
        String res = null;
        byte[] buf = new byte[65536];
        int num_read;
        InputStream in = new ByteArrayInputStream(cadena.getBytes());

        try {
            java.security.MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            while ((num_read = in.read(buf)) != -1) {
                digest.update(buf, 0, num_read);
            }
            res = asHex(digest.digest()); //+"  "+filename;
            in.close();
        } catch (IOException | NoSuchAlgorithmException ex) {
        }
        return res;
    }

    /**
     * Turns array of bytes into string representing each byte as unsigned hex
     * number.
     *
     * @param hash	Array of bytes to convert to hex-string
     * @return	Generated hex string
     */
    private static String asHex(byte hash[]) {
        char buf[] = new char[hash.length * 2];
        for (int i = 0, x = 0; i < hash.length; i++) {
            buf[x++] = HEX_CHARS[(hash[i] >>> 4) & 0xf];
            buf[x++] = HEX_CHARS[hash[i] & 0xf];
        }
        return new String(buf);
    }

}
