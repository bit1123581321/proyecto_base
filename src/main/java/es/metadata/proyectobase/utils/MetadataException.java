package es.metadata.proyectobase.utils;

public class MetadataException extends Exception{

    public MetadataException(String message) {
        super(message);
    }

    public MetadataException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
