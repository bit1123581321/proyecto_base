package es.metadata.proyectobase.utils;

import es.metadata.proyectobase.security.UsuarioConectado;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Properties;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.omnifaces.util.Beans;

@Intercepts({
    @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
    @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
public class ChangeDatabaseMyBatisPlugin implements Interceptor, Serializable {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
               
        UsuarioConectado usuario = Beans.getInstance(UsuarioConectado.class);
        
        Executor executor = (Executor) invocation.getTarget();
        Connection connection = executor.getTransaction().getConnection();                
        connection.setCatalog(usuario.getEsquema());

        return invocation.proceed();
    }
    
    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }
  
    @Override
    public void setProperties(Properties properties) {
    }
}
