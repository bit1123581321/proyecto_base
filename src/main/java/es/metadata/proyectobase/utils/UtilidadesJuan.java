/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.metadata.proyectobase.utils;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Persona;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.services.PersonaService;
import es.metadata.proyectobase.services.UsuarioService;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jange
 */
@Named("utilidadesJuan")
@ApplicationScoped

public class UtilidadesJuan implements Serializable {

    @Inject
    UsuarioService usuarioService;

    @Inject
    PersonaService personaService;

    public int insertarUserConPersona(String nombre, String apellidos, String dni, String password, String email) throws MetaLoginException {
        //0 --> Todo ok
        //1 --> Persona ya existente con usuario
        //2 --> Email ya existente
        //3 --> Persona existe sin usuario (se le asigna)
        int opcion = 0;
        if (dniNoExistente(dni)) {
            if (emailNoExistente(email)) {
                Usuario user = new Usuario();
                user.setPassword(password);
                user.setEmail(email);

                Persona per = new Persona();
                per.setApellidos(apellidos);
                per.setNombre(nombre);
                per.setDni(dni);
                personaService.insertar(per);
                per = personaService.obtenerPersonaPorDNI(dni);

                user.setPersona(per);
                usuarioService.insertarConPersona(user);
            } else {
                opcion = 2;
            }
        } else if(esPersonaSinUsuario(dni)){
             Usuario user = new Usuario();
                user.setPassword(password);
                user.setEmail(email);

                Persona per = personaService.obtenerPersonaPorDNI(dni);
                per.setApellidos(apellidos);
                per.setNombre(nombre);
                personaService.actualizar(per);

                user.setPersona(per);
                usuarioService.insertarConPersona(user);
                opcion = 3;
        }
        else{
            opcion = 1;
        }
        return opcion;
    }

    //El validador no está implementado, para empezar solo comprueba que no esté repetido
    public boolean dniNoExistente(String dni) {
        boolean ok = true;
        List<Persona> lista = personaService.obtenerTodos();
        for (Persona per : lista) {
            if (dni.equals(per.getDni())) {
                ok = false;
                return ok;
            }
        }
        return ok;
    }

    public boolean esPersonaSinUsuario(String dni) throws MetaLoginException {
        boolean ok = true;
        Persona per = personaService.obtenerPersonaPorDNI(dni);
        if (per != null) {
            List<Usuario> lista = usuarioService.obtenerTodosConPersona();
            for (Usuario us : lista) {
                if (us.getPersona().getDni().equals(dni)) {
                    ok = false;
                    return ok;
                }
            }
        }
        return ok;
    }

    public boolean dniNoExistenteMenosTuyo(String dni, Persona per) {
        boolean ok = true;
        List<Persona> lista = personaService.obtenerTodos();
        for (Persona persona : lista) {
            if (!per.getDni().equals(dni) && dni.equals(persona.getDni())) {
                ok = false;
                return ok;
            }
        }
        return ok;
    }

    //El validador no está implementado, para empezar solo comprueba que no esté repetido
    public boolean emailNoExistente(String email) {
        boolean ok = true;
        List<Usuario> lista = usuarioService.obtenerTodos();
        for (Usuario us : lista) {
            if (email.equals(us.getEmail())) {
                ok = false;
                return ok;
            }
        }
        return ok;
    }

    public boolean emailNoExistenteMenosTuyo(String email, Usuario usuario) {
        boolean ok = true;
        List<Usuario> lista = usuarioService.obtenerTodos();
        for (Usuario us : lista) {
            if (!usuario.getEmail().equals(email) && email.equals(us.getEmail())) {
                ok = false;
                return ok;
            }
        }
        return ok;
    }

    public int validarEdicion(String passw, String passwNueva, String passwNueva2, String email, String dni, Usuario user) {
        int resul = 0;
        //0 todo Ok sin cambio de contraseña
        //1 contraseña incorrecta
        //2 email incorrecto
        //3 DNI incorrecto
        //4 Quiere cambiar de contraseña pero está mal
        //5  Todo Ok con cambio de contraseña

        if (!passw.equals(user.getPassword())) {
            return 1;
        } else if (!emailNoExistenteMenosTuyo(email, user)) {
            return 2;
        } else if (!dniNoExistenteMenosTuyo(dni, user.getPersona())) {
            return 3;
        } else if (!passwNueva.equals("")) {
            if (passwNueva.equals(passwNueva2)) {
                return 5;
            }
            return 4;
        }
        return resul;
    }
}
