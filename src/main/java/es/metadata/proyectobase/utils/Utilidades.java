package es.metadata.proyectobase.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.commons.io.IOUtils;
import org.omnifaces.util.Messages;

@Named("utilidades")
@ApplicationScoped
public class Utilidades {
    public static <T> T[] appendToArray(T[] arr, T element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }

    public static String facesRedirectConMensaje(String outcome) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        if (outcome.contains("?")) {
            return outcome + "&faces-redirect=true";
        } else {
            return outcome + "?faces-redirect=true";
        }
    }

    /**
     * Redirecciona a la pagina especificada con un mensaje que se mostrara en
     * el componente "growl" si este existe
     *
     * @param URL a la que navegar
     * @param params parametros dentro de la URL
     * @param mensaje a mostrar
     */
    public static void redireccionConMensaje(String URL, String params, String mensaje) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(URL
                    + (!params.equalsIgnoreCase("") ? "?" + params + "&mensaje=" + mensaje
                    : "?mensaje=" + mensaje));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Muestra un mensaje en el componente messages
     *
     * @param severidad tipo de mensaje a mostrar. Los posibles mensajes son:
     * info, error.
     */
    public static void mostrarMensajeEnMessages(String severidad) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        Map parametrosUrl = ec.getRequestParameterMap();

        if ((String) parametrosUrl.get("mensaje") != null) {
            switch (severidad) {
                case "info":
                    Messages.addGlobalInfo((String) parametrosUrl.get("mensaje"));
                    break;
                case "error":
                    Messages.addGlobalError((String) parametrosUrl.get("mensaje"));
                    break;
            }
        }
    }

    /**
     * Redirige a la pagina pasada como parametro
     *
     * @param pagina Nombre de la página sin la extensión
     */
    public static void facesRedirect(String pagina) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(pagina + ".xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static String localDateToString(LocalDate fecha) {
        if (fecha != null) {
            return fecha.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        } else {
            return "";
        }
    }

    public static String localDateTimeToString(LocalDateTime fecha) {
        if (fecha != null) {
            return fecha.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        } else {
            return "";
        }
    }

    public static LocalDateTime dateToLocalDateTime(Date fecha) {
        return new java.sql.Timestamp(fecha.getTime()).toLocalDateTime();
    }
    
    public static Date localDateTimeToDate(LocalDateTime d) {
        return Date.from(d.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static String getHostname() {
        String hostname = "Unknown";

        try {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException ex) {
            System.out.println("Hostname can not be resolved");
        }
        return hostname;
    }

    public static void copiarArchivo(File origen, File destino) {
        InputStream in;
        FileOutputStream out;

        if (destino.exists()) {
            destino.delete();
        }

        try {
            in = new FileInputStream(origen);
            out = new FileOutputStream(destino);

            IOUtils.copy(in, out);
            out.close();
        } catch (FileNotFoundException ex) {
//            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(CompraService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Nota: Usar esta función sólo cuando el número de espacios es muy grande
     * (Por motivos de eficiencia) Esta función se encarga de quitar todos los
     * espacios de una cadena. Tanto iniciales, como finales, como los que estén
     * por medio
     *
     * @param cadena String con la cadena con espacios
     * @return String con la cadena sin espacios
     */
    public static String quitarEspaciosAppend(String cadena) {
        String[] arrayCadena = cadena.split(" ");
        String cadenaSinEspacios = "";

        for (String array : arrayCadena) {
            cadenaSinEspacios = new StringBuilder(cadenaSinEspacios).append(array).toString();
        }

        return cadenaSinEspacios;
    }

    /**
     * Nota: Usar esta función sólo cuando el número de espacios es pequeño (Por
     * motivos de eficiencia) Esta función se encarga de quitar todos los
     * espacios de una cadena. Tanto iniciales, como finales, como los que estén
     * por medio
     *
     * @param cadena String con la cadena con espacios
     * @return String con la cadena sin espacios
     */
    public static String quitarEspaciosPlus(String cadena) {
        return cadena.replaceAll("\\s+", "");
    }

    /**
     * Formatea los segundos pasados como parametros para mostrarlos como horas,
     * minutos y segundos
     *
     * @param seg
     * @return
     */
    public static String formatearSegundosEnHoras(Integer seg) {
        Integer horas;
        Integer minutos;
        Integer segundos;

        horas = seg / 3600;
        minutos = (seg - (3600 * horas)) / 60;
        segundos = seg - ((horas * 3600) + (minutos * 60));

        return String.format(String.format("%%0%dd", 2), horas) + ":" + String.format(String.format("%%0%dd", 2), minutos) + ":" + String.format(String.format("%%0%dd", 2), segundos);
    }

    /**
     * Formatea los segundos pasados como parametros para mostrarlos como
     * minutos y segundos
     *
     * @param seg
     * @return
     */
    public static String formatearSegundosEnMinutos(Integer seg) {
        Integer minutos;
        Integer segundos;

        minutos = seg / 60;
        segundos = seg - (minutos * 60);

        return String.format(String.format(String.format("%%0%dd", 2), minutos) + ":" + String.format(String.format("%%0%dd", 2), segundos));
    }
}
