package es.metadata.proyectobase.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Encriptacion {

    private String clave;

    public Encriptacion() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Encriptacion(String clave) {
        this.clave = Hash.calcular(clave);
    }

    public String encriptar(String message) {
        try {
            byte[] keyMaterial = hexByte(clave);
            SecretKeySpec key = new SecretKeySpec(keyMaterial, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plaintext = message.getBytes("UTF-8");
            byte[] ciphertext = cipher.doFinal(plaintext);
            return asHex(ciphertext);
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
            ex.printStackTrace();
            return message;
        }
    }

    public String desencriptar(String message) {
        try {
            byte[] keyMaterial = hexByte(clave);
            SecretKeySpec key = new SecretKeySpec(keyMaterial, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] original = cipher.doFinal(hexByte(message));
            return new String(original, "UTF-8");
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
            return "";
        }
    }

    /**
     * Turns array of bytes into string
     *
     * @param buf	Array of bytes to convert to hex string
     * @return Generated hex string
     */
    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    private static byte[] hexByte(final String encoded) {
        if ((encoded.length() % 2) != 0) {
            throw new IllegalArgumentException("Input string must contain an even number of characters");
        }

        final byte result[] = new byte[encoded.length() / 2];
        final char enc[] = encoded.toCharArray();
        for (int i = 0; i < enc.length; i += 2) {
            StringBuilder curr = new StringBuilder(2);
            curr.append(enc[i]).append(enc[i + 1]);
            result[i / 2] = (byte) Integer.parseInt(curr.toString(), 16);
        }
        return result;
    }

    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("Procedimiento para encriptar y desencriptar palabras utilizando el algoritmo AES");
                System.out.println("Encriptacion (metodo,palabra,clave)");
                System.out.println("    metodo = 'e' se utiliza para encriptar");
                System.out.println("    metodo = 'd' se utiliza para desencriptar");
                System.out.println("    palabra es la palabra que queremos encriptar o desencriptar");
                System.out.println("    clave es la palabra que queremos utilizar para encriptar y desencriptar");
            } else if (args.length != 3) {
                System.out.println("El numero de parametros no es correcto");
            } else {
                if (!args[0].equalsIgnoreCase("e") && !args[0].equalsIgnoreCase("d")) {
                    System.out.println("El primer parametro no es correcto");
                } else {
                    Encriptacion ae = new Encriptacion(args[2]);
                    if (args[0].equalsIgnoreCase("e")) {
                        System.out.println(ae.encriptar(args[1]));
                    } else {
                        System.out.println(ae.desencriptar(args[1]));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
