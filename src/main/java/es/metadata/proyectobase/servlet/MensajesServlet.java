package es.metadata.proyectobase.servlet;

import es.metadata.proyectobase.providers.MessageFactoryProvider;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.json.JSONObject;

@WebServlet(urlPatterns = "/mensajes")
public class MensajesServlet extends AbstractFacesServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processRequest(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processRequest(request, response);
    }

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        Instant instant = Instant.parse("2099-12-31T01:00:00.0z");
        response.setDateHeader("Expires", instant.toEpochMilli());
        response.setHeader("Cache-Control", "max-age=7200, public");
        response.setHeader("Pragma", "cache");

        FacesContext facesContext = this.getFacesContext(request, response);
        ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");

        Map<String, String> msj = resourceBundleToMap(bundle);
        msj = msj.entrySet().stream().filter(p -> p.getKey().startsWith("js_")).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        JSONObject json = new JSONObject(msj);

        try (PrintWriter out = response.getWriter()) {
            out.append("var msg = " + json.toString());
        }
    }

    private static Map<String, String> resourceBundleToMap(final ResourceBundle bundle) {
        final Map<String, String> bundleMap = new HashMap<>();

        for (String key : bundle.keySet()) {
            final String value = bundle.getString(key);

            bundleMap.put(key, value);
        }

        return bundleMap;
    }
}
