package es.metadata.proyectobase.conponents;

import javax.faces.component.UINamingContainer;

public class MetaTable extends UINamingContainer {

    private Object selection;

    public Object getSelection() {
        return selection;
    }

    public void setSelection(Object selection) {
        this.selection = selection;
    }

    public Object dummyAction() {
        return null;
    }

}
