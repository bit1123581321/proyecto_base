package es.metadata.proyectobase.providers;

import es.metadata.proyectobase.constantes.TipoHost;
import es.metadata.proyectobase.controllers.ApplicationBean;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.cdi.SessionFactoryProvider;

public class SqlSessionMetaLoginFactory {

    @Inject
    private ApplicationBean applicationBean;

    private InputStream reader;

    @PostConstruct
    public void init() {
        reader = SqlSessionMetaLoginFactory.class.getResourceAsStream("/es/metadata/proyectobase/configuracion/mybatisproyectovacio.xml");
    }

    @Produces
    @ApplicationScoped
    @SessionFactoryProvider
    public SqlSessionFactory produceSqlSession() throws IOException {
        org.apache.ibatis.session.SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, "development");
        return sqlSessionFactory;
    }
}
