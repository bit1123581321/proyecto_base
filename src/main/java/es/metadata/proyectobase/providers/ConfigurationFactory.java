package es.metadata.proyectobase.providers;

import es.metadata.proyectobase.utils.Utilidades;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

@ApplicationScoped
public class ConfigurationFactory {

    private static final String PREFIJO_PC_DESARROLLO = "metadata";
    private static final String PROPS_FILENAME_FORMAT = "/es/metadata/proyectobase/configuracion/configuration.properties";
    private static final String PROPS_FILENAME_FORMAT_DESARROLLO = "/es/metadata/proyectobase/configuracion/configuration_desarrollo.properties";
    private Properties environmentProps;

    @PostConstruct
    public void initEnvironmentProps() throws Exception {
        environmentProps = new Properties();

        InputStream inputStream = ConfigurationFactory.class.getResourceAsStream(PROPS_FILENAME_FORMAT);
        if (inputStream == null) {
            throw new FileNotFoundException("Properties file for environment not found in the classpath.");
        }
        environmentProps.load(inputStream);

        String host = environmentProps.getProperty("host_produccion");
        int esHostProduccion = esHostProduccion(host);

        if (esHostProduccion != 1) {
            //cargo entonces el fichero de propiedades de desarrollo
            inputStream = ConfigurationFactory.class.getResourceAsStream(PROPS_FILENAME_FORMAT_DESARROLLO);
            if (inputStream == null) {
                throw new FileNotFoundException("Properties file for environment not found in the classpath.");
            }
            environmentProps.load(inputStream);
        } else if (esHostProduccion == -1) {
            throw new RuntimeException("Host no reconocido");
        }
    }

    @Produces
    @Configuration
    public String getConfigValue(InjectionPoint ip) {
        Configuration config = ip.getAnnotated().getAnnotation(Configuration.class);
        String configKey = config.value();
        if (configKey.isEmpty()) {
            throw new IllegalArgumentException("Properties value key is required.");
        }
        return environmentProps.getProperty(configKey);
    }

    /**
     * Comprueba cual es el host donde se ha desplegado la aplicacion
     *
     * @param host_produccion
     * @return 1 si es en un servidor de produccion, 2 si es un servidor de
     * desarrollo y 3 si es el servidor de Jenkins
     */
    public int esHostProduccion(String host_produccion) {
        int esproduccion = 3;
        String[] hostnames_produccion = host_produccion.split(";");

        if (Utilidades.getHostname().toLowerCase().contains(PREFIJO_PC_DESARROLLO)) {
            esproduccion = 2;
            return esproduccion;
        }

        for (String host : hostnames_produccion) {
            if (Utilidades.getHostname().equals(host)) {
                esproduccion = 1;
                break;
            }
        }

        return esproduccion;
    }
}
