package es.metadata.proyectobase.providers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import org.apache.ibatis.session.SqlSessionFactory;

@ApplicationScoped
public class MessageFactoryProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    private ResourceBundle bundle;
    private InputStream mensajesBundle;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context != null) {
            bundle = context.getApplication().getResourceBundle(context, "msg");
        } else {
            mensajesBundle = SqlSessionFactory.class.getResourceAsStream("/es/metadata/proyectobase/messages/messages_es.properties");
            try {
                bundle = new PropertyResourceBundle(new InputStreamReader(mensajesBundle, "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(MessageFactoryProvider.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MessageFactoryProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getValue(String key) {
        String result;
        try {
            result = bundle.getString(key);
        } catch (MissingResourceException e) {
            result = "???" + key + "??? not found";
        }
        return result;
    }

    public String getValue(String key, Object... params) {
        try {
            return MessageFormat.format(bundle.getString(key), params);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
