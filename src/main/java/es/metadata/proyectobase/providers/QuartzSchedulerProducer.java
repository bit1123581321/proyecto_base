package es.metadata.proyectobase.providers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import org.apache.deltaspike.scheduler.spi.Scheduler;
import org.quartz.Job;

public class QuartzSchedulerProducer {

    @Produces
    @ApplicationScoped
    protected Scheduler<Job> produceScheduler(Scheduler scheduler) {
        return scheduler;
    }
}
