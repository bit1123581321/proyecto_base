package es.metadata.proyectobase.providers;

import java.io.InputStream;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class LoggerFactoryProvider {

    private InputStream log4jproperties;

    @PostConstruct
    public void init() {
        log4jproperties = SqlSessionMetaLoginFactory.class.getResourceAsStream("/es/metadata/proyectobase/configuracion/log4j.properties");

    }

    @Produces
    public Logger produceLogger(InjectionPoint injectionPoint) {
        PropertyConfigurator.configure(log4jproperties);
        return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }
}
