package es.metadata.proyectobase.modelExt;

import java.util.HashMap;

public class ColumnasAdicionales {
    private HashMap<String, Object> valoresCalculados;
    
    public HashMap<String, Object> getValoresCalculados() {
        return valoresCalculados;
    }

    public void setValoresCalculados(HashMap<String, Object> valoresCalculados) {
        this.valoresCalculados = valoresCalculados;
    }
}
