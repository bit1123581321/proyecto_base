package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.services.UsuarioService;
import es.metadata.proyectobase.utils.UtilidadesJuan;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

@Named("registroController")
@ViewScoped

public class RegistroController extends Base {

    @Inject
    UsuarioService usuarioService;

    @Inject
    UtilidadesJuan utilidadesJuan;

    private String email;
    private String password;
    private String password2;
    private String nombre;
    private String apellidos;
    private String dni;
    private String mensaje;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String ok = request.getParameter("ok");
        FacesMessage message = new FacesMessage();
        if (ok != null) {
            if (ok.equals("0")) {
                mensaje = "Usuario creado con exito, ahora logueate.";
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje);
            } else if (ok.equals("1")) {
                mensaje = "Otro usuario ya usa tu DNI";
                message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje);
            } else if (ok.equals("2")) {
                mensaje = "Email ya existente";
                message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje);
            } else if (ok.equals("3")) {
                mensaje = "Se ha detectado que tienes una cuenta previa, la hemos vinculado.";
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje);
            } else if (ok.equals("4")) {
                mensaje = "Contraseñas no coinciden";
                message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje);
            }

            PrimeFaces.current().dialog().showMessageDynamic(message);
        }
    }

    public String registrar() throws MetaLoginException {
        int ok = 4;//No coinciden las passwords
        if (password.equals(password2)) {
            ok = utilidadesJuan.insertarUserConPersona(nombre, apellidos, dni, password, email);
        }
        return "registro?ok=" + ok + "&?faces-redirect=true";
    }

    public String volverLogin() {
        return "login?faces-redirect=true";
    }

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters">
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    //</editor-fold>
}
