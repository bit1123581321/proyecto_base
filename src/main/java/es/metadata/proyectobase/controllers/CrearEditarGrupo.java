package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Grupo;
import es.metadata.proyectobase.model.Persona;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.security.UsuarioConectado;
import es.metadata.proyectobase.services.GrupoService;
import es.metadata.proyectobase.services.PersonaService;
import es.metadata.proyectobase.services.UsuarioService;
import es.metadata.proyectobase.utils.UtilidadesJuan;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.DualListModel;

@Named("crearEditarGrupo")
@ViewScoped
public class CrearEditarGrupo implements Serializable {

    @Inject
    GruposController gruposController;

    @Inject
    UsuarioService usuarioService;

    @Inject
    GrupoService grupoService;

    @Inject
    UtilidadesJuan utilidadesJuan;

    private Grupo grupo;

    private String mensaje;

    private boolean edicion;

    private boolean error;

    private DualListModel<String> usuarios;

    @PostConstruct
    public void init() {
        List<String> dentro = new ArrayList();
        List<String> fuera = new ArrayList();

        grupo = gruposController.getGrupo();

        if (grupo != null) {
            edicion = true;
            List<Usuario> dentroU = usuarioService.obtenerPorGrupo(grupo.getIdGrupo());
            List<Usuario> fueraU = usuarioService.obtenerPorNoGrupo(grupo.getIdGrupo());
            for (Usuario u : dentroU) {
                dentro.add(u.getEmail());
            }
            for (Usuario u : fueraU) {
                String previo = u.getGrupo().getNombre();
                fuera.add(u.getEmail()+"; previamente en el grupo "+ previo);
            }

        } else {
            grupo = new Grupo();
            edicion = false;
            List<Usuario> fueraU = usuarioService.obtenerTodosConGrupo();
            for (Usuario u : fueraU) {
                String previo = u.getGrupo().getNombre();
                fuera.add(u.getEmail()+"; previamente en el grupo "+ previo);
            }
        }

        usuarios = new DualListModel<>(dentro, fuera);

        mensaje = "";
    }

    public void botonClickado() throws MetaLoginException {
        if (edicion) {
            editarGrupo();
        } else {
            insertarGrupo();
        }
    }

    public void editarGrupo() throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        List<Grupo> total = grupoService.obtenerTodos();
        if (!grupo.getNombre().equals("") && !existePreviamente(grupoService.obtenerGrupoPorId(grupo.getIdGrupo()).getNombre(), grupo.getNombre(), total)) {
            grupoService.actualizar(grupo);
            traspasarUsuarios();
            mensaje = "Grupo modificado con exito";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje));
        } else {
            mensaje = "Nombre de grupo en uso";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }

    }
    
    public void traspasarUsuarios() throws MetaLoginException{
        for (String us : usuarios.getSource()) {
                us = us.split(";")[0];
                Usuario aux = usuarioService.obtenerUsuarioPorCorreo(us);
                aux.setGrupo(grupo);
                usuarioService.actualizarConGrupo(aux);
            }
            List<Usuario> lista = usuarioService.obtenerPorGrupo(grupo.getIdGrupo());
            List<String> listaS = new ArrayList();
            for (Usuario u : lista) {
                listaS.add(u.getEmail());
            }
            for (String us : usuarios.getTarget()) {
                us = us.split(";")[0];
                if (listaS.contains(us)) {
                    Usuario aux = usuarioService.obtenerUsuarioPorCorreo(us);
                    aux.setGrupo(null);
                    usuarioService.actualizarConGrupo(aux);
                }
            }
    }

    public boolean existePreviamente(String anterior, String actual, List<Grupo> grupos) {
        boolean ok = false;
        for (Grupo g : grupos) {
            if (actual.equals(g.getNombre()) && !anterior.equals(actual)) {
                ok = true;
                return ok;
            }
        }

        return ok;
    }

    public void insertarGrupo() throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (!grupo.getNombre().equals("")) {
            grupoService.insertar(grupo);
            traspasarUsuarios();
            mensaje = "Grupo creado con exito";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje));
        } else {
            mensaje = "Nombre de grupo en uso";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }

    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isEdicion() {
        return edicion;
    }

    public void setEdicion(boolean edicion) {
        this.edicion = edicion;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DualListModel<String> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(DualListModel<String> usuarios) {
        this.usuarios = usuarios;
    }

}
