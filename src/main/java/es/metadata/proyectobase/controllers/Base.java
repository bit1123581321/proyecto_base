package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.providers.MessageFactoryProvider;
import es.metadata.proyectobase.security.UsuarioConectado;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;

public class Base implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected SessionBean sessionBean;

    @Inject
    protected ApplicationBean applicationBean;

    @Inject
    protected UsuarioConectado usuarioConectado;

    @Inject
    protected MessageFactoryProvider mensajes;

    @Inject
    protected transient Logger log;

    @Inject
    HttpSession httpsession;

    public List<Integer> getPermisosUsuario() {
        return usuarioConectado.getPermisos();
    }

    @PostConstruct
    public void inicializar() {
    }

}
