package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.services.UsuarioService;
import es.metadata.proyectobase.utils.UtilidadesJuan;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

@Named("loginController")
@RequestScoped

public class LoginController extends Base {

    @Inject
    UsuarioService usuarioService;

    private String usuario;
    private String password;

    @PostConstruct
    public void init() {
    }

    public void procesarParametros() {
    }

    public void comprobar() throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (loginCorrecto()) {
                usuarioConectado.setNombreUsuario(usuario);
                httpsession.setAttribute("usuarioConectado", usuarioConectado);
                Faces.redirect("dashboard.xhtml");

            } else {
                log.debug(mensajes.getValue("mensaje0001"));
                Messages.addGlobalError(mensajes.getValue("mensaje0001"));
                usuario = "";
                password = "";
               // context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Credenciales incorrectas"));
            }
        } catch (IOException ex) {
            usuario = "";
            password = "";
            log.error(mensajes.getValue("mensaje0002"), ex);
            Messages.addGlobalError(mensajes.getValue("mensaje0002"));
            //context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Credenciales incorrectas"));
        }
    }
    
    public String registrar() throws MetaLoginException {
        return "registro?faces-redirect=true";
    }

    public String cerrarSesion() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }

    public boolean loginCorrecto() throws MetaLoginException {
        boolean ok = false;
        Usuario user = usuarioService.obtenerUsuarioPorCorreo(usuario);

        if (user != null && password.equals(user.getPassword())) {
            ok = true;
        }

        return ok;

    }

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters">
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public UsuarioService getUsuarioService() {
        return usuarioService;
    }
    
    //</editor-fold>

    
}
