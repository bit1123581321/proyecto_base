package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Grupo;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.services.GrupoService;
import es.metadata.proyectobase.services.UsuarioService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named("gruposController")
@RequestScoped
public class GruposController implements Serializable {

    @Inject
    private UsuarioService usuarioService;
    
    @Inject
    private GrupoService grupoService;
    
    private Grupo grupo;

    private List<Grupo> grupos;

    private String mensaje;

    @PostConstruct
    public void init() {
        this.grupos = grupoService.obtenerTodos();
        mensaje = "";
    }
    
    public int obtenerUsuarios(Grupo grup){
        return usuarioService.contarPorGrupo(grup.getIdGrupo());
    }

    public void eliminarGrupo(Grupo grup) throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (true) { //personaLogueada.esAdmin
            grupoService.borrar(grup.getIdGrupo());
            mensaje = "Grupo eliminado con exito";
            grupos.remove(grup);
            context.addMessage(null, new FacesMessage("Correcto", mensaje));
        } else {
            mensaje = "No tienes permisos";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }
    }

    public String crearEditar(Grupo grup) throws MetaLoginException {
        grupo= grup;
        return "crearEditarGrupo";
    }

    public List<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }    
    
}
