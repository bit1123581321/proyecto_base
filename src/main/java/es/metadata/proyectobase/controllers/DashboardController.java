package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Fichada;
import es.metadata.proyectobase.model.TipoFichada;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.security.UsuarioConectado;
import es.metadata.proyectobase.services.FichadaService;
import es.metadata.proyectobase.services.TipoFichadaService;
import es.metadata.proyectobase.services.UsuarioService;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.time.DateUtils;
import org.omnifaces.cdi.ViewScoped;

@Named("dashboardController")
@ViewScoped
public class DashboardController implements Serializable {

    @Inject
    UsuarioConectado usuarioConectado;

    @Inject
    UsuarioService usuarioService;

    @Inject
    FichadaService fichadaService;

    @Inject
    TipoFichadaService tipoFichadaService;

    Usuario user;

    List<Fichada> fichadas;

    List<TipoFichada> tipoFichadas;

    String mensaje;

    boolean activo;

    @PostConstruct
    public void init() {
        try {
            try {
                user = usuarioService.obtenerUsuarioPersonaPorCorreo(usuarioConectado.getNombreUsuario());
                fichadas = fichadaService.obtenerTodoDelUserNoActivas(user.getIdUsuario());
                tipoFichadas = tipoFichadaService.obtenerTodos();
            } catch (MetaLoginException ex) {
                Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
            }
            mensaje = "";
            activo = fichadaService.obtenerFichadaPorActivo(user) != null;
        } catch (MetaLoginException ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void nuevaFichada(TipoFichada tipo) throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (fichadaService.obtenerFichadaPorActivo(user) != null) {
            mensaje = "Acabas de fichar, termina primero lo que estas haciendo";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        } else {
            Fichada fichada = new Fichada();
            fichada.setDuracion(0);
            fichada.setFechaInicio(new Date(System.currentTimeMillis()));
            fichada.setTipoFichada(tipo);
            fichada.setUsuario(user);
            fichadaService.insertar(fichada);
            fichadas.add(fichada);
            mensaje = "Has fichado con exito";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje));
        }
        activo = true;

    }

    public String tiempoEscrito() throws ParseException, MetaLoginException {
        Fichada ultima = fichadaService.obtenerFichadaPorActivo(user);
        long tiempo = 0;
        String escrito = "Actualmente lleva: ";
        if (ultima != null) {

            escrito = escrito + ultima.tiempoEscrito();
        } else {
            escrito = "Aun no ha empezado";
        }
        return escrito;

    }

    public void finFichada() throws MetaLoginException, ParseException {
        FacesContext context = FacesContext.getCurrentInstance();
        Fichada ultima = fichadaService.obtenerFichadaPorActivo(user);
        if (ultima != null) {
            ultima.setDuracion((int) ultima.tiempo());
            fichadaService.actualizar(ultima);
            mensaje="Has fichado con exito";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", mensaje));

        }
        activo = false;

    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List<Fichada> getFichadas() {
        return fichadas;
    }

    public void setFichadas(List<Fichada> fichadas) {
        this.fichadas = fichadas;
    }

    public List<TipoFichada> getTipoFichadas() {
        return tipoFichadas;
    }

    public void setTipoFichadas(List<TipoFichada> tipoFichadas) {
        this.tipoFichadas = tipoFichadas;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

}
