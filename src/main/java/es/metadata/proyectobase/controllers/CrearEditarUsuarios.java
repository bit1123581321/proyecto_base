package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Persona;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.security.UsuarioConectado;
import es.metadata.proyectobase.services.PersonaService;
import es.metadata.proyectobase.services.UsuarioService;
import es.metadata.proyectobase.utils.UtilidadesJuan;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.omnifaces.cdi.ViewScoped;

@Named("crearEditarUsuarios")
@ViewScoped
public class CrearEditarUsuarios implements Serializable {

    @Inject
    UsuariosController usuariosController;

    @Inject
    UsuarioService usuarioService;

    @Inject
    PersonaService personaService;

    @Inject
    UtilidadesJuan utilidadesJuan;
    
    @Inject
    HttpSession httpsession;
    
    @Inject
    protected UsuarioConectado usuarioConectado;

    private Usuario user;

    private String pssw;

    private String psswNueva;

    private String psswNueva2;

    private String mensaje;

    private boolean edicion;

    private boolean error;

    @PostConstruct
    public void init() {
        this.user = usuariosController.getUser();
        if (user != null) {
            edicion = true;
        } else {
            user = new Usuario();
            Persona per = new Persona();
            user.setPersona(per);
            edicion = false;
        }

        mensaje = "";
        pssw = "";
        psswNueva = "";
        psswNueva2 = "";
    }

    public void botonClickado() throws MetaLoginException {
        if (edicion) {
            editarUser();
        } else {
            insertarUser();
        }
        pssw = "";
        psswNueva = "";
        psswNueva2 = "";

    }

    public void editarUser() throws MetaLoginException {
        //Cogemos el usuario anterior para la comprobación, por eso lo tenemos que llamar de la BBDD
        int opcion = utilidadesJuan.validarEdicion(pssw, psswNueva, psswNueva2,
                user.getEmail(), user.getPersona().getDni(), usuarioService.obtenerUsuarioConPersona(user.getIdUsuario()));
        FacesContext context = FacesContext.getCurrentInstance();

        String msj = "datos actualizados";
        error = true;
        switch (opcion) {
            case 0:
                usuarioService.actualizar(this.user);
                personaService.actualizar(this.user.getPersona());
                error = false;
                usuarioConectado.setNombreUsuario(user.getEmail());
                httpsession.setAttribute("usuarioConectado", usuarioConectado);
                break;
            case 1:
                msj = "Contraseña incorrecta";
                break;
            case 2:
                msj = "Email incorrecto";
                break;
            case 3:
                msj = "DNI ya existe";
                break;
            case 4:
                msj = "Contraseñas no coinciden";
                break;
            case 5:
                user.setPassword(psswNueva);
                usuarioService.actualizar(this.user);
                personaService.actualizar(this.user.getPersona());
                error = false;
                usuarioConectado.setNombreUsuario(user.getEmail());
                httpsession.setAttribute("usuarioConectado", usuarioConectado);
                break;
        }
        this.mensaje = msj;
        if (error) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        } else {
            context.addMessage(null, new FacesMessage("Correcto", mensaje));
        }
    }

    public void insertarUser() throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        Persona per = user.getPersona();
        int i = utilidadesJuan.insertarUserConPersona(per.getNombre(), per.getApellidos() , per.getDni(), pssw, user.getEmail());

        if (i==1) {
            mensaje="Persona ya existente";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }
        else if (i==2) {
            mensaje="Email ya existente";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }else {
            mensaje="Usuario insertado con exito";
            context.addMessage(null, new FacesMessage("Correcto", mensaje));
            
        }

    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public String getPssw() {
        return pssw;
    }

    public void setPssw(String pssw) {
        this.pssw = pssw;
    }

    public String getPsswNueva() {
        return psswNueva;
    }

    public void setPsswNueva(String psswNueva) {
        this.psswNueva = psswNueva;
    }

    public String getPsswNueva2() {
        return psswNueva2;
    }

    public void setPsswNueva2(String psswNueva2) {
        this.psswNueva2 = psswNueva2;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isEdicion() {
        return edicion;
    }

    public void setEdicion(boolean edicion) {
        this.edicion = edicion;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}
