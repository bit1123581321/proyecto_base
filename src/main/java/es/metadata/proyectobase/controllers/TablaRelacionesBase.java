package es.metadata.proyectobase.controllers;

import java.io.Serializable;

public class TablaRelacionesBase extends Base implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String MOSTRAR_TODOS = "T";
    public static final String MOSTRAR_SELECCIONADOS = "S";

    protected Boolean checkSeleccionarTodoMarcado;

    protected String mostrarFilas;

    public Boolean getCheckSeleccionarTodoMarcado() {
        return checkSeleccionarTodoMarcado;
    }

    public void setCheckSeleccionarTodoMarcado(Boolean checkSeleccionarTodoMarcado) {
        this.checkSeleccionarTodoMarcado = checkSeleccionarTodoMarcado;
    }

    public String getMostrarFilas() {
        return mostrarFilas;
    }

    public void setMostrarFilas(String mostrarFilas) {
        this.mostrarFilas = mostrarFilas;
    }

    protected void recargarTabla() {
        checkSeleccionarTodoMarcado = !MOSTRAR_TODOS.equals(mostrarFilas);
    }
}
