package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.constantes.TipoHost;
import es.metadata.proyectobase.providers.Configuration;
import es.metadata.proyectobase.utils.Utilidades;
import java.io.Serializable;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.deltaspike.scheduler.spi.Scheduler;
import org.omnifaces.cdi.Eager;
import org.quartz.Job;
import org.slf4j.Logger;

@Named("applicationBean")
@ApplicationScoped
@Eager
public class ApplicationBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String PREFIJO_PC_DESARROLLO = "metadata";

    @Inject
    private transient Logger log;

    @Inject
    @Configuration("host_cron")
    private String host_cron;

    @Inject
    @Configuration("host_produccion")
    private String host_produccion;

    @Inject
    @Configuration("host_jenkins")
    private String host_jenkins;

    private static String key_priv = "_Paivdaju";

    private String fechaDespliegue;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    public String getKey_priv() {
        return key_priv;
    }

    // <editor-fold defaultstate="collapsed" desc="Get & Set">
    public String getFechaDespliegue() {
        return fechaDespliegue;
    }

    public void setFechaDespliegue(String fechaDespliegue) {
        this.fechaDespliegue = fechaDespliegue;
    }
    // </editor-fold>

    @Inject
    private Scheduler<Job> jobScheduler;

    @PostConstruct
    public void init() {
        log.info("!--- LANZAMIENTO CRON ProyectoBase ---!");
        log.info("Cron lanzados para el host: " + Utilidades.getHostname());

        fechaDespliegue = sdf.format(new Date());

        if (Utilidades.getHostname().equals(host_cron)) {
//            !--- AQUÍ INICIAMOS LOS CRON. LA LÍNEA DE ABAJO ES UN EJEMPLO ---!
//            this.jobScheduler.registerNewJob(ActualizaDatosMetaContratas.class);
        } else {
            log.info("Error al lanzar el cron para el host: " + Utilidades.getHostname());
        }
    }

    /**
     * Comprueba cual es el host donde se ha desplegado la aplicacion
     *
     * @return Un valor del enumero dependiendo del host: PRODUCCION, DESARROLLO
     * o JENKINS (Por defecto)
     */
    public TipoHost tipoHost() {
        TipoHost tipoHost = TipoHost.JENKINS;
        String[] hostnames_produccion = host_produccion.split(";");

        if (Utilidades.getHostname().toLowerCase().contains(PREFIJO_PC_DESARROLLO)) {
            tipoHost = TipoHost.DESARROLLO;
        }

        for (String host : hostnames_produccion) {
            if (Utilidades.getHostname().equals(host)) {
                tipoHost = TipoHost.PRODUCCION;
                break;
            }
        }

        return tipoHost;
    }

    public boolean esHostProduccion() {
        return tipoHost().equals(TipoHost.PRODUCCION);
    }

    public boolean esHostDesarrollo() {
        return tipoHost().equals(TipoHost.DESARROLLO);
    }
}
