package es.metadata.proyectobase.controllers;

import es.metadata.proyectobase.exceptions.MetaLoginException;
import es.metadata.proyectobase.model.Usuario;
import es.metadata.proyectobase.services.UsuarioService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named("usuariosController")
@RequestScoped
public class UsuariosController implements Serializable {

    @Inject
    private UsuarioService usuarioService;

    private Usuario user;

    private List<Usuario> usuarios;

    private String mensaje;

    @PostConstruct
    public void init() {
        this.usuarios = usuarioService.obtenerTodosConPersona();
        this.user = new Usuario();
        mensaje = "";
    }

    public void eliminarUsuario(Usuario us) throws MetaLoginException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (true) { //personaLogueada.esAdmin
            usuarioService.borrar(us.getIdUsuario());
            mensaje = "Usuario eliminado con exito";
            usuarios.remove(us);
            context.addMessage(null, new FacesMessage("Correcto", mensaje));
        } else {
            mensaje = "No tienes permisos";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
        }
    }

    public String crearEditar(Usuario us) throws MetaLoginException {
        user = us;
        return "crearEditarUsuario";
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
