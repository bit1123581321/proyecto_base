package es.metadata.proyectobase.controllers;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;

@Named("menuController")
@ViewScoped
//@SecurityRol(rolesPermitidos = {Rol.ADMINISTRADOR, Rol.CLIENTE, Rol.DIOS})
public class MenuController extends Base {

    private static final long serialVersionUID = 1L;


    @PostConstruct
    private void init() {
    }
}
