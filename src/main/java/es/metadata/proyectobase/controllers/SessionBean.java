package es.metadata.proyectobase.controllers;

import java.io.Serializable;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@SessionScoped
public class SessionBean implements Serializable {

    private static final long serialVersionUID = 1L;       
    
    private String locale;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @PostConstruct
    public void init(){
        Locale idioma = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        locale = idioma.getLanguage();
    }
}
