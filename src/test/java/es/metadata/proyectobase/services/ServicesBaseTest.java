package es.metadata.proyectobase.services;

import es.metadata.proyectobase.security.Rol;
import es.metadata.proyectobase.security.UsuarioConectado;
import java.io.File;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public abstract class ServicesBaseTest {

    @Inject
    private UsuarioConectado usuarioConectado;

    private static boolean usuarioConectadoCreado = false;

    //Ayuda1: https://developer.jboss.org/thread/252501
    //Ayuda2: https://stackoverflow.com/questions/42728129/how-to-mock-mybatis-mapper-interface-with-arquillian-part2
    //Ejemplo de referencia: https://github.com/zappee/mybatis-arquillian-embeddedserver-test
    /*
    * Deployment: El propósito de un archivo de pruebas es aislar las clases y recursos que son necesarios por la prueba del resto del
    classpath. A diferencia de una prueba unitaria normal, Arquillian no simplemente se sumerge dentro del classpath. En
    lugar de eso, incluye solamente lo que la prueba necesita (que puede ser todo el classpath, si así lo decides). El archivo
    se define utilizando ShrinkWrap, que es un API en Java para la creación de archivos (e.g., jar, war, ear). La estrategia de
    micro-despliegues te permite enfocarte exactamente en las clases que quieres probar y, como resultado, la prueba permanece muy pequeña.
     */
    @Deployment
    public static WebArchive createDeployment() {
        //Esto coge el war generado y lo usa para hacer las pruebas, por lo que tiene que existir el war.
        WebArchive archive = ShrinkWrap.create(ZipImporter.class, "proyectobase.war").importFrom(new File("target/proyectobase.war")).as(WebArchive.class);

        //Esto carga las dependencias del mybatis. Si lo quitamos, no encuentra el SQLSessionFactory
        String key = "org.mybatis:mybatis-cdi";
        JavaArchive[] libraries = Maven.resolver().loadPomFromFile("pom.xml").resolve(key).withTransitivity().as(JavaArchive.class);
        archive.addAsLibraries(libraries);

        key = "org.apache.deltaspike.modules:deltaspike-scheduler-module-impl";
        libraries = Maven.resolver().loadPomFromFile("pom.xml").resolve(key).withTransitivity().as(JavaArchive.class);
        archive.addAsLibraries(libraries);
        // archive.addAsResource(new File("src/main/webapp/WEB-INF/mybatis.xml"), "mybatis.xml");
        // archive.addAsResource(new File("src/main/webapp/WEB-INF/log4j.properties"), "log4j.properties");
        //Esto es para que añada todos los paquetes de la aplicación a los tests, y olvidarnos de cuál usa esta clase.
        //Pongo el paquete general es/metadata porque al poner true como primer parámetro se cargan todos sus subpaquetes.
        archive.addPackages(true, "es/metadata");

        return archive;
    }

    @Rule
    public TestWatcher observadorTest = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            System.out.println(description.getDisplayName() + " ha fallado: " + e.getMessage());
            super.failed(e, description);
        }

        @Override
        protected void succeeded(Description description) {
            System.out.println(description.getDisplayName() + " ha terminado satisfactoriamente");
            super.succeeded(description);
        }

    };

    // Run once, e.g. Database connection, connection pool
    @BeforeClass
    public static void runOnceBeforeClass() {

    }

    @Before
    public void simularUsuarioConectado() {
        if (!usuarioConectadoCreado) {
            usuarioConectado.setUsuario("USUARIO_PRUEBA");
            usuarioConectado.setRol(Rol.ADMINISTRADOR);
            usuarioConectado.setModo_dios(true);
            usuarioConectado.setIdUsuario(0);
            usuarioConectadoCreado = true;
        }
    }

    @After
    public void eliminarUsuarioConectado() {
        usuarioConectadoCreado = false;
    }

    // Should rename to @BeforeTestMethod
    // e.g. Creating an similar object and share for all @Test
    @Before
    public abstract void ejecutarAntesDeCadaTest();

    // Should rename to @AfterTestMethod
    @After
    public abstract void ejecutarDespuesDeCadaTest();

}
